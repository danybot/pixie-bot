package com.danybot.Diagnosis.System;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.danybot.Diagnosis.DiagnosisStatus;
import com.danybot.Diagnosis.DiagnosisSubject;
import com.danybot.Diagnosis.Troubleshoot;
import com.danybot.Model.Entity;
import com.danybot.Model.Entities.TestEntity;

@Component
public class Storage extends Troubleshoot {

	@Override
	public DiagnosisStatus start() {

		Random random = new Random();

		// Prepare test data.
		int randomInt = random.nextInt();
		float randomFloat = random.nextFloat();
		double randomDouble = random.nextDouble();
		String randomString = String.valueOf(randomDouble);

		List<String> randomList = new ArrayList<String>();
		Map<String, Object> randomMap = new HashMap<String, Object>();
		for (int count = 0; count < 10; count++) {
			UUID randomUUID = UUID.randomUUID();
			randomList.add(randomUUID.toString());
			randomMap.put(String.valueOf(count), randomUUID.toString());
		}

		TestEntity insertTestEntity = Entity.access().getTestEntity();
		insertTestEntity.setIntegerValue(randomInt);
		insertTestEntity.setFloatValue(randomFloat);
		insertTestEntity.setDoubleValue(randomDouble);
		insertTestEntity.setStringValue(randomString);
		insertTestEntity.setListValue(randomList);
		insertTestEntity.setMapValue(randomMap);

		Entity.save();
		Entity.refresh();

		TestEntity retrivedTestEntity = Entity.access().getTestEntity();
		if (retrivedTestEntity.getIntegerValue() != randomInt)
			return DiagnosisStatus.FAILED;
		if (retrivedTestEntity.getFloatValue() != randomFloat)
			return DiagnosisStatus.FAILED;
		if (retrivedTestEntity.getDoubleValue() != randomDouble)
			return DiagnosisStatus.FAILED;
		if (!randomString.equals(retrivedTestEntity.getStringValue()))
			return DiagnosisStatus.FAILED;

		List<String> listValue = retrivedTestEntity.getListValue();
		if (listValue.size() != randomList.size())
			return DiagnosisStatus.FAILED;
		for (int count = 0; count < listValue.size(); count++) {
			if (!listValue.get(count).equals(randomList.get(count)))
				return DiagnosisStatus.FAILED;
		}

		Map<String, Object> mapValue = retrivedTestEntity.getMapValue();
		if (mapValue.size() != randomMap.size())
			return DiagnosisStatus.FAILED;
		for (String key : mapValue.keySet()) {
			if (!mapValue.get(key).equals(randomMap.get(key)))
				return DiagnosisStatus.FAILED;
		}

		return DiagnosisStatus.SUCCESS;
	}

	@Override
	public DiagnosisSubject getSubject() {
		return DiagnosisSubject.SYSTEM;
	}

	@Override
	public String getTopic() {
		return "Storage";
	}

}
