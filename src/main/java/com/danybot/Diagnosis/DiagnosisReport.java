package com.danybot.Diagnosis;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DiagnosisReport {

	private DiagnosisStatus status;

	private DiagnosisSubject subject;

	private String topic;

}
