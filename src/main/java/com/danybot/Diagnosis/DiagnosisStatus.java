package com.danybot.Diagnosis;

public enum DiagnosisStatus {

	SUCCESS, FAILED, TIMEOUT;

	public String toString() {
		return this.name();
	}

}
