package com.danybot.Diagnosis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.danybot.CommonUtils.SystemUtils;
import com.danybot.Engine.BeanUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Diagnosis {

	public static boolean isRunning = false;

	public static DiagnosisSubject subjectRunning;

	public static List<DiagnosisReport> run() {
		return run(null);
	}

	public static List<DiagnosisReport> run(DiagnosisSubject subject) {
		List<DiagnosisReport> reportList = new ArrayList<DiagnosisReport>();
		if (isRunning) {
			log.warn("Full Diagnosis is in Progress");
		} else {
			isRunning = true;
			Map<String, Troubleshoot> topics = BeanUtil.getBeansOfType(Troubleshoot.class);
			for (Map.Entry<String, Troubleshoot> topic : topics.entrySet()) {
				if (Objects.isNull(subject) || (topic.getValue().getSubject() == subject)) {
					reportList.add(runTopic(topic.getValue()));
				}
			}
			isRunning = false;
		}
		return reportList;
	}

	private static DiagnosisReport runTopic(Troubleshoot topic) {
		System.out.println();
		log.info("Running Test : {}", topic.toString());
		DiagnosisReport report = DiagnosisReport.builder()
				.status(topic.start())
				.subject(topic.getSubject())
				.topic(topic.getTopic()).build();
		log.info("Completed Test : {} : {}", topic.toString(), report.getStatus());
		return report;
	}
}
