package com.danybot.Diagnosis;

public enum DiagnosisSubject {

	SYSTEM,
	INTERFACE;

	public String toString() {
		return this.name();
	}
}
