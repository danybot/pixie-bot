package com.danybot.Diagnosis;

public abstract class Troubleshoot {

	/**
	 * Run the test activity and return the result of the test.
	 * 
	 * @return {boolean}
	 */
	public abstract DiagnosisStatus start();

	/**
	 * Subject that test case Belongs.
	 * 
	 * @return {Subject}
	 */
	public abstract DiagnosisSubject getSubject();

	/**
	 * Topic inside the subject
	 * 
	 * @return {String}
	 */
	public abstract String getTopic();

	/**
	 * return formatted subject and topic
	 * 
	 * @return {String}
	 */
	public String toString() {
		return getSubject().toString() + ":" + getTopic();
	}
}
