package com.danybot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.danybot.Diagnosis.Diagnosis;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		preConfigure(args);
		SpringApplication.run(Application.class, args);
		postConfigure(args);
	}

	public static void preConfigure(String[] args) {
		log.info("Pre configuring");
		System.err.close();
		System.setErr(System.out);
	}

	public static void postConfigure(String[] args) {
		log.info("Post configuring");
		Diagnosis.run();
	}
}
