package com.danybot.Model;

import java.util.Objects;

import lombok.Getter;

@Getter
public class Context {

	private static Context context;

	/**
	 * Holder to handle all the static cache memory, At the end of session all will
	 * not be saved. It is good practice to maintaining the context as an non null
	 * fields;
	 * 
	 * @return {Entity}
	 */
	public static Context access() {
		if (Objects.isNull(context)) {
			context = new Context();
		}
		return context;
	}
}
