package com.danybot.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.danybot.CommonUtils.FileAccessor;
import com.danybot.Model.Entities.TestEntity;
import com.danybot.Model.Entities.UserAccountEntity;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class Entity implements Serializable {

	private static final long serialVersionUID = -5287898317449386269L;

	private TestEntity testEntity = new TestEntity();

	private List<UserAccountEntity> userAccountEntity = new ArrayList<UserAccountEntity>();

	private static Entity entity;

	/**
	 * Holder to handle all the cache memory, At the end of session all will be
	 * saved. If there is no cache then it will Instantiate and return the object.
	 * 
	 * @return {Entity}
	 */
	public static Entity access() {
		if (Objects.isNull(entity)) {
			log.info("Retrieve Cache data");
			entity = FileAccessor.read(Entity.class.getName(), Entity.class);
			if (Objects.isNull(entity))
				entity = new Entity();
		}
		return entity;
	}

	/**
	 * Save Entity by serializing. It will proceed only entity is non null.
	 */
	public static void save() {
		FileAccessor.write(Entity.class.getName(), entity);
	}

	/**
	 * refresh the entity by accessing It again
	 */
	public static void refresh() {
		entity = null;
		access();
	}
}
