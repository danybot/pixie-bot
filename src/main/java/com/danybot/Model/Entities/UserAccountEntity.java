package com.danybot.Model.Entities;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserAccountEntity implements Serializable {

	private static final long serialVersionUID = 203961593829779821L;

	private String firstName;

	private String lastName;

	private String UserId;

	private String emailId;

	private String encryptedPassword;

	private AccountType role = AccountType.GUEST;

	public enum AccountType {
		MASTER, ADMIN, GUEST,
	}
}
