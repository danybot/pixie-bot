package com.danybot.Model.Entities;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class TestEntity implements Serializable {

	private static final long serialVersionUID = 5034316549501405998L;

	private int integerValue;

	private String stringValue;

	private float floatValue;

	private double doubleValue;
	
	private boolean booleanValue;

	List<String> listValue;

	Map<String, Object> mapValue;

	private int anotherInteger;
}
