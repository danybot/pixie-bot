package com.danybot.WebInterface.Config;

import java.util.List;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.danybot.Model.Entity;
import com.danybot.Model.Entities.UserAccountEntity;
import com.danybot.Model.Entities.UserAccountEntity.AccountType;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/lib/**", "/images/**")
				.permitAll()
				.anyRequest()
				.authenticated().and()
				.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/index", false)
				.permitAll()
				.and()
				.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/login")
				.permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		List<UserAccountEntity> userDetail = Entity.access().getUserAccountEntity();

		// For Develop default login as an Master
		// TODO Need to be removed [1000V DANGER]
		if (userDetail.isEmpty()) {
			UserAccountEntity masterAccount = new UserAccountEntity();
			masterAccount.setFirstName("balaji");
			masterAccount.setLastName("dhanapal");
			masterAccount.setRole(AccountType.MASTER);
			masterAccount.setEmailId("sample@smail.com");
			masterAccount.setUserId("admin");
			masterAccount.setEncryptedPassword(this.passwordEncoder().encode("admin"));
			userDetail.add(masterAccount);
			Entity.save();
		}

		for (UserAccountEntity userAccount : userDetail) {
			auth.inMemoryAuthentication()
					.passwordEncoder(this.passwordEncoder())
					.withUser(userAccount.getUserId())
					.password(userAccount.getEncryptedPassword())
					.roles(userAccount.getRole().name());
		}
	}

	private PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
